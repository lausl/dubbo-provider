package com.dubbo.provider;

import org.apache.log4j.BasicConfigurator;

/**
 * @Author: Ethan Lau
 * @Date: 2019/4/24 15:13
 * @Description: TODO
 * @ClassName: Main
 * @version: v1.0
 **/
public class Main {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        com.alibaba.dubbo.container.Main.main(args);
    }
}
