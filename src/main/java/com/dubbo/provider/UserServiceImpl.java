package com.dubbo.provider;

import com.dubbo.api.UserService;

/**
 * @Author: Ethan Lau
 * @Date: 2019/4/24 15:14
 * @Description: TODO
 * @ClassName: UserServiceImpl
 * @version: v1.0
 **/
public class UserServiceImpl implements UserService {
    public String sayHi(String s) {
        return "Hello " + s + "!";
    }
}
